FROM ubuntu:20.04

LABEL Author="François Trahay <francois.trahay@telecom-sudparis.eu>"
LABEL Title="EZIOTracer in Docker"

# Enviorment variables
ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Install dependencies
RUN apt-get update && apt-get install -y \ 
	wget \ 
	git \
	cmake \
 	ninja-build\
	make \
	gcc-10 \
	python3-pip \
	g++-10 \
	gfortran \
	linux-headers-generic\
	bpftrace \
	pkg-config

# install meson
RUN pip3 install meson

# instal eziotracer
RUN wget https://gitlab.com/idiom1/eziotrace/-/raw/master/install_eziotrace.sh \
&& bash install_eziotrace.sh

# Set the working directory
WORKDIR /