#!/bin/bash

input_path="$PWD/path_input"
output_path="$PWD/path_output"

set -x 
docker run -d -it --rm --volume "$input_path":/tmp/input --volume "$output_path":/tmp/output nytimes/blender bash -c "blender -b /tmp/input/bouncing_ball.blend --render-output /tmp/output/frame_##### --render-format PNG --engine CYCLES --render-frame 10"
