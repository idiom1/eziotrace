# Example Blender

## Building the docker image

First, you need to build the `eziotracer` image (see ../README.md).
Then create the `blender_eziotracer` docker image that contains both EZIOTracer and blender:


```
docker build . -t blender_eziotracer
```

## Running Bender with EZIOTracer

The script `run_docker_eziotracer_blender.sh` runs Blender with
EZTrace on an example. EZTrace instruments blender and records a trace
in `path_output/blender_trace/`.