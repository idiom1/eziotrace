#!/bin/bash

# by default, build/install everything in the current directory
ROOT_DIR="$PWD"

# run make in parallel with 4 concurrent processes
MAKE="make -j4"

# if set to yes, git uses ssh to fetch data from the server
GIT_CLONE_SSH=no

if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` [absolute/path/to/build_and_install/dir/]"
  exit 0
fi

system_install=yes # should software be installed in default directories (eg. /usr/bin) ? 

if [ $# -ne 0 ]; then
    if [[ "$1" = /* ]]; then
	# you can also pass a prefix
	ROOT_DIR="$1"
    else
	echo "Build and installation directory must be an absolute path"
	exit 0
    fi
    system_install=no
fi

ROOT_INSTALL_DIR="$ROOT_DIR/install"
ROOT_SRC_DIR="$ROOT_DIR/src"

if [ "$system_install" = "no" ]; then
    mkdir -p "$ROOT_INSTALL_DIR"
fi
mkdir -p "$ROOT_SRC_DIR"

# check dependancies

meson_version=$(meson --version)
meson_version_major=$(echo $meson_version |awk -F. '{print $1}')
meson_version_minor=$(echo $meson_version |awk -F. '{print $2}')
meson_version_rev=$(echo $meson_version |awk -F. '{print $3}')
if [ "$meson_version_major" -eq 0 ] &&
       [ "$meson_version_minor" -lt 56 ]; then
    echo "meson version 0.56 or higher is required" >&2
    exit 1
fi


#IOTRacer
IOTRACER_SRC_ROOT="$ROOT_SRC_DIR/iotracer"
export IOTRACER_ROOT="$ROOT_INSTALL_DIR/iotracer"

IOTRACER_SSH_GIT=git@github.com:medislam/bpftrace_iotracer_like.git
IOTRACER_HTTPS_GIT=https://github.com/medislam/bpftrace_iotracer_like.git
IOTRACER_GIT="$IOTRACER_HTTPS_GIT"
if [ "$GIT_CLONE_SSH" = "yes" ]; then IOTRACER_GIT="$IOTRACER_SSH_GIT" ; fi

if ! [ -d "$IOTRACER_ROOT" ]; then
    echo "Installing iotracer..."
    rm -rf "$IOTRACER_ROOT"
    git clone "$IOTRACER_GIT" "$IOTRACER_ROOT"|| exit 1
else
    echo "iotracer is already installed"
fi


# install otf2
OTF2_SRC_ROOT="$ROOT_SRC_DIR/otf2"
export OTF2_ROOT="$ROOT_INSTALL_DIR/otf2"

if ! [ -d "$OTF2_ROOT" ]; then
    echo "Installing otf2..."
    rm -rf "$OTF2_SRC_ROOT"
    wget http://perftools.pages.jsc.fz-juelich.de/cicd/otf2/tags/otf2-2.3/otf2-2.3.tar.gz || exit 1
    tar xf otf2-2.3.tar.gz || exit 1
    mv otf2-2.3 "$OTF2_SRC_ROOT" || exit 1

    cd "$OTF2_SRC_ROOT" || exit 1

    if [ "$system_install" = "no" ]; then
	prefix="--prefix=$OTF2_ROOT"
    else
	prefix="--bindir=/usr/local/bin   --libdir=/usr/local/lib  --includedir=/usr/local/include"
    fi
    ./configure $prefix || exit 1
    $MAKE install|| exit 1
else
    echo "otf2 is already installed"
fi


# install eztrace
EZTRACE_SRC_ROOT="$ROOT_SRC_DIR/eztrace"
export EZTRACE_ROOT="$ROOT_INSTALL_DIR/eztrace"
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$EZTRACE_ROOT/lib/pkgconfig
export NEW_PKG_CONFIG_PATH=$EZTRACE_ROOT/lib/pkgconfig

# uncomment to use ssh for git clone
EZTRACE_SSH_GIT=git@gitlab.com:eztrace/eztrace.git
EZTRACE_HTTPS_GIT=https://gitlab.com/eztrace/eztrace.git
EZTRACE_GIT="$EZTRACE_HTTPS_GIT"
if [ "$GIT_CLONE_SSH" = "yes" ]; then EZTRACE_GIT="$EZTRACE_SSH_GIT" ; fi
    
if ! [ -d "$EZTRACE_ROOT" ]; then
    echo "Installing eztrace..."
    rm -rf "$EZTRACE_SRC_ROOT"
    git clone -b eztrace-2.0 "$EZTRACE_GIT" "$EZTRACE_SRC_ROOT"|| exit 1
    cd "$EZTRACE_SRC_ROOT" || exit 1

    if [ "$system_install" = "no" ]; then
	prefix="-DCMAKE_INSTALL_PREFIX=$EZTRACE_ROOT"
    else
	OTF2_PATH="-DOTF2_PATH=$OTF2_ROOT"
    fi
    cmake $prefix  -DENABLE_BIN_INSTRUMENTATION=no -DEZTRACE_ENABLE_IOTRACER=yes -DIOTRACER_PATH="${IOTRACER_ROOT}/bcc_iotracer.py" -DEZTRACE_ENABLE_POSIXIO=yes  $OTF2_PATH  . || exit 1
    
    make -j4 install|| exit 1
else
    echo "eztrace is already installed"
fi

# ETA

ETA_SRC_ROOT="$ROOT_SRC_DIR/eta"
export ETA_ROOT="$ROOT_INSTALL_DIR/eta"
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$ETA_ROOT/lib/x86_64-linux-gnu/pkgconfig
export NEW_PKG_CONFIG_PATH=$NEW_PKG_CONFIG_PATH:$ETA_ROOT/lib/x86_64-linux-gnu/pkgconfig

ETA_SSH_GIT=git@gitlab.com:parallel-and-distributed-systems/easytraceanalyzer/eta_trace.git
ETA_HTTPS_GIT=https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_trace.git
ETA_GIT="$ETA_HTTPS_GIT"
if [ "$GIT_CLONE_SSH" = "yes" ]; then ETA_GIT="$ETA_SSH_GIT" ; fi

if ! [ -d "$ETA_ROOT" ]; then
    echo "Installing eta..."
    rm -rf "$ETA_SRC_ROOT"
    git clone -b trace_merger "$ETA_GIT" "$ETA_SRC_ROOT"|| exit 1
    cd "$ETA_SRC_ROOT" || exit 1

    if [ "$system_install" = "no" ]; then
	prefix="-Dprefix=$ETA_ROOT"
	otf2_incdir="-Dotf2_incdir=$OTF2_ROOT/include"
	otf2_libdir="-Dotf2_libdir=$OTF2_ROOT/lib"
    else
	otf2_incdir="-Dotf2_incdir=/usr/local/include"
	otf2_libdir="-Dotf2_libdir=/usr/local/lib"
    fi

    meson build $prefix -Dotf2=enabled $otf2_incdir $otf2_libdir || exit 1
    cd build || exit 1
    ninja install|| exit 1
else
    echo "eta is already installed"
fi


# ETA merge
ETA_MERGE_SRC_ROOT="$ROOT_SRC_DIR/eta_merge"
export ETA_MERGE_ROOT="$ROOT_INSTALL_DIR/eta_merge"

ETA_MERGE_SSH_GIT=git@gitlab.com:parallel-and-distributed-systems/easytraceanalyzer/eta_merge.git
ETA_MERGE_HTTPS_GIT=https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_merge.git
ETA_MERGE_GIT="$ETA_MERGE_HTTPS_GIT"
if [ "$GIT_CLONE_SSH" = "yes" ]; then ETA_MERGE_GIT="$ETA_MERGE_SSH_GIT" ; fi

if ! [ -d "$ETA_MERGE_ROOT" ]; then
    echo "Installing eta_merge..."
    rm -rf "$ETA_MERGE_SRC_ROOT"
    git clone "$ETA_MERGE_GIT" "$ETA_MERGE_SRC_ROOT"|| exit 1
    cd "$ETA_MERGE_SRC_ROOT" || exit 1

    if [ "$system_install" = "no" ]; then
	prefix=" -DCMAKE_INSTALL_PREFIX=$ETA_MERGE_ROOT"
    fi

    cmake $prefix . || exit 1
    
    make -j4 install|| exit 1
else
    echo "eta_merge is already installed"
fi


# ETA profile
ETA_PROFILE_SRC_ROOT="$ROOT_SRC_DIR/eta_profile"
export ETA_PROFILE_ROOT="$ROOT_INSTALL_DIR/eta_profile"

ETA_PROFILE_SSH_GIT=git@gitlab.com:parallel-and-distributed-systems/easytraceanalyzer/eta_profile.git
ETA_PROFILE_HTTPS_GIT=https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_profile.git
ETA_PROFILE_GIT="$ETA_PROFILE_HTTPS_GIT"
if [ "$GIT_CLONE_SSH" = "yes" ]; then ETA_PROFILE_GIT="$ETA_PROFILE_SSH_GIT" ; fi

if ! [ -d "$ETA_PROFILE_ROOT" ]; then
    echo "Installing eta_profile..."
    rm -rf "$ETA_PROFILE_SRC_ROOT"
    git clone "$ETA_PROFILE_GIT" "$ETA_PROFILE_SRC_ROOT"|| exit 1
    cd "$ETA_PROFILE_SRC_ROOT" || exit 1

    if [ "$system_install" = "no" ]; then
	prefix="-Dprefix=$ETA_PROFILE_ROOT"
    fi

    meson build $prefix  || exit 1
    cd build || exit 1
    ninja install|| exit 1
else
    echo "eta_profile is already installed"
fi

if [ "$system_install" = "no" ]; then

    SET_ENV_SCRIPT="$ROOT_INSTALL_DIR/set_env.sh"

    echo "#!/bin/bash" > "$SET_ENV_SCRIPT"
    echo "" >> "$SET_ENV_SCRIPT"
    echo export OTF2_ROOT=\"$OTF2_ROOT\"  >> "$SET_ENV_SCRIPT"
    echo export EZTRACE_ROOT=\"$EZTRACE_ROOT\" >> "$SET_ENV_SCRIPT"
    echo export IOTRACER_ROOT=\"$IOTRACER_ROOT\" >> "$SET_ENV_SCRIPT"
    echo export ETA_ROOT=\"$ETA_ROOT\" >> "$SET_ENV_SCRIPT"
    echo export ETA_MERGE_ROOT=\"$ETA_MERGE_ROOT\" >> "$SET_ENV_SCRIPT"
    echo export ETA_PROFILE_ROOT=\"$ETA_PROFILE_ROOT\"  >> "$SET_ENV_SCRIPT"
    echo "" >> "$SET_ENV_SCRIPT"
    echo export PKG_CONFIG_PATH=\"\$PKG_CONFIG_PATH:$NEW_PKG_CONFIG_PATH\" >> "$SET_ENV_SCRIPT"
    echo export PATH=\$PATH:\$OTF2_ROOT/bin:\$EZTRACE_ROOT/bin:\$ETA_ROOT/bin:\$ETA_MERGE_ROOT/bin:\$ETA_PROFILE_ROOT/bin >> "$SET_ENV_SCRIPT"
    chmod +x "$SET_ENV_SCRIPT"

    echo "Installation complete in $ROOT_INSTALL_DIR"
    echo "You can add the following lines to your .bashrc:"
    echo ""
    echo "source $SET_ENV_SCRIPT"
fi
