#!/bin/bash


IOTRACER=$(which iotracer.sh)

data_dir=data_dir

input_file=data_dir/file_10k
output_file=$data_dir/output_file

cmd="./my_cat_fd $input_file $output_file"
if [ $# -gt 0 ]; then
    cmd=$@
fi


echo "running:"
echo "sudo $IOTRACER -d $data_dir $cmd"
sudo $IOTRACER -d $data_dir $cmd

