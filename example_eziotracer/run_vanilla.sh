#/bin/bash

input_file=data_dir/file_10k
output_file=data_dir/output_file

cmd="./my_cat_fd $input_file $output_file"
if [ $# -gt 0 ]; then
    cmd=$@
else
    make my_cat_fd
fi

echo "running:"
echo "$cmd"
$cmd
