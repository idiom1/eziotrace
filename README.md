# EZIOTrace


EZIOTrace is a multi-level IO Tracer that detects IO events from both user space and kernel space.

It relies on several pieces of software, including:
* [EZTrace](https://gitlab.com/eztrace/eztrace): for tracing user-space IO events
* [IOTracer](https://gitlab.com/eztrace/eztrace):  for tracing kernel-space IO events
* [EasyTraceAnalyzer](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer): for post-mortem processing of traces


## Installing dependencies


```
sudo apt install python3-pip g++-10 cmake git  ninja-build make gcc linux-headers-generic bpfcc-tools pkg-config
sudo pip3 install meson
```

## Installing EZIOTrace

Since EZIOTrace relies on several pieces of software, we provide an
installing script that download all the required software component and install them:

```
./install_eziotrace.sh [install_path]
```

## Using EZTrace

Once installed, running `eztrace_avail` should show that the `posixio` module is available:
```
$ eztrace_avail 
Available modules:
lib     "Module for pthread functions"
pthread "Module for pthread synchronization functions (mutex, spinlock, etc.)"
**posixio** "Module for posix IO functions (fread, fwrite, read, write, etc.)"
mpi     "Module for MPI functions"
```

You can then run an application with EZTrace:
```
$ eztrace -t posixio ./application param1 param2
[P0T0] Starting EZTrace (pid: 41165)...
[...]
\* Application runs happily \*
[...]
[P0T0] Stopping EZTrace (pid:41165)...
```

For MPI applications, you need to select several eztrace modules:
```
$ mpirun -np 4 eztrace -t "mpi posixio" application
[P1T0] Starting EZTrace (pid: 41342)...
[P2T0] Starting EZTrace (pid: 41343)...
[P3T0] Starting EZTrace (pid: 41344)...
[P0T0] Starting EZTrace (pid: 41341)...

[...]
\* Application runs happily \*
[...]

[P2T0] Stopping EZTrace (pid:41343)...
[P3T0] Stopping EZTrace (pid:41344)...
[P1T0] Stopping EZTrace (pid:41342)...
[P0T0] Stopping EZTrace (pid:41341)...
```

This collects IO events in an OTF2 trace file located in directory
`[application]_trace`.

### Printing the content of a trace

You can print the generated trace with `otf2-print`:

```
$ otf2-print application_trace/eztrace_log.otf2

=== OTF2-PRINT ===
=== Events =====================================================================
Event                               Location            Timestamp  Attributes
--------------------------------------------------------------------------------
ENTER                                      0    10569470564644516  Region: "Working" <0>
ENTER                                      0    10569470564758346  Region: "open" <5>
IO_CREATE_HANDLE                           0    10569470564771360  Handle: "timer.flag" <0>, Access Mode: READ_WRITE, Creation Flags: NONE, Status Flags: {CLOSE_ON_EXEC}
LEAVE                                      0    10569470564774282  Region: "open" <5>
ENTER                                      0    10569470564975076  Region: "write" <10>
[...]
```

### Analyzing a trace file

You can get a profile of the application with `eta_profile`:
```
$ eta_profile -g bt.W.4_trace/eztrace_log.otf2 
Opening bt.W.4_trace/eztrace_log.otf2
     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
         4         4.6585           100%                  Working
      9648       0.103192       2.21513%                mpi_wait_
      9672      0.0564977       1.21279%               mpi_isend_
       808      0.0452132      0.970553%             mpi_waitall_
      9672     0.00713199      0.153096%               mpi_irecv_
         4     0.00343597      0.073757%           MPI_Comm_split
         8     0.00340813     0.0731593%             MPI_Comm_dup
         8     0.00140169     0.0300889%             mpi_barrier_
        24     0.00118822     0.0255066%               mpi_bcast_
         8    0.000316409    0.00679208%           mpi_allreduce_
        67     0.00021432    0.00460061%                    write
         4    8.09971e-05    0.00173869%              mpi_reduce_
         2    1.90847e-05   0.000409674%                     open

      Type     Count  Duration     % runtime
----------.---------.---------.-------------
     Other        73   4.65874      100.005%
       MPI     29856  0.221866      4.76261%

Time spent in P2P MPI communications: 0.489795: 10.514% runtime

Time spent in Collective MPI communications: 0.00295649: 0.0634644% runtime

Total duration: 4.6585
```

## Running EZTrace and IOTracer

IOTracer captures the kernel IO operation on a file or a directory.
You need to specify the inode of the file or directory to watch:

```
input_file=.
export IOTRACER_TRACED_INODE=$(ls -id $(dirname "$input_file") |cut -f1 -d" ")
export IOTRACER_TRACED_FILTER=dir
export IOTRACER_STATE=ENABLED
```

Once the 3 variables are set, you can run eztrace:

```
$ eztrace -t posixio ./application param1 param2
[P0T0] Starting EZTrace (pid: 41165)...
IOtracer: ENABLED
[...]
\* Application runs happily \*
[...]
[P0T0] IOtracer: converting IOtracer log file (/tmp/output/iotracer_trace.txt) to otf2 format (/tmp/output/IOtracer_output)
[P0T0] Stopping EZTrace (pid:41165)...
```

This generates several traces:
- `application_trace` contains EZTrace events of what happens in user-space (format: OTF2)
- `IOtracer_output` contains IOTracer events of what happens in kernel-space   (format: OTF2)
- `iotracer_trace.txt` contains IOTrace events (format: text)

### Merging traces

You can merge EZTrace and IOTracer OTF2 traces into one trace that
contains both user and kernel space events:

```
$ eta_merge -m application_trace/eztrace_log.otf2  IOtracer_output/log.otf2
```

This generates a trace names `output/trace.otf2`


## Building the docker image

We provide a `Dockerfile` for creating a docker container with EZIOTracer installed.

```
docker build . -t eziotracer
```
